﻿using System;
using System.Collections.Generic;

namespace FormulaOneBusiness
{
    public class Team
    {
        private string fullTeamName;
        private string teamBase;
        private string teamChief;
        private int worldChampionship;
        private List<Driver> drivers;
        private List<Car> cars;

        public Team(string fullTeamName, string teamBase, string teamChief)
        {
            this.fullTeamName = fullTeamName;
            this.teamBase = teamBase;
            this.teamChief = teamChief;
        }

        public string FullTeamName
        {
            get
            {
                return this.fullTeamName;
            }
        }

        public string TeamBase
        {
            get
            {
                return this.teamBase;
            }
        }

        public string TeamChief
        {
            get
            {
                return this.teamChief;
            }
        }


        public int WorldChampionship
        {
            get
            {
                return this.worldChampionship;
            }

            set
            {
                this.worldChampionship = value;
            }
        }

        public List<Driver> Drivers
        {
            get
            {
                return this.drivers;
            }

            set
            {
                this.drivers = value;
            }
        }

        public List<Car> Cars
        {
            get
            {
                return this.cars;
            }

            set
            {
                this.cars = value;
            }
        }
    }
}
