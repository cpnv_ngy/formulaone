﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FormulaOneBusiness;

namespace TestFormulaOneBusiness
{
    [TestClass]
    public class TestDriver
    {
        [TestMethod]
        public void Name_GetValue_Success()
        {
            //given
            Driver driver = new Driver("Hamilton", "Lewis", 44);
            string actualValue = "";
            string expectedValue = "Hamilton";

            //when
            actualValue = driver.Name;

            //then
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void Lastname_GetValue_Success()
        {
            //given
            Driver driver = new Driver("Hamilton", "Lewis", 44);
            string actualValue = "";
            string expectedValue = "Lewis";

            //when
            actualValue = driver.Lastname;

            //then
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void Number_GetValue_Success()
        {
            //given
            Driver driver = new Driver("Hamilton", "Lewis", 44);
            int actualValue;
            int expectedValue = 44;

            //when
            actualValue = driver.Number;

            //then
            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
