﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FormulaOneBusiness;
using System.Collections.Generic;

namespace TestFormulaOneBusiness
{
    [TestClass]
    public class TestTeam
    {
        [TestMethod]
        public void FullTeamName_GetValue_Success()
        {
            //given
            Team team = new Team("Mercedes-AMG Petronas F1 Team", "Brackley, United Kingdom", "Toto Wolff");
            string actualValue = "";
            string expectedValue = "Mercedes-AMG Petronas F1 Team";

            //when
            actualValue = team.FullTeamName;

            //then
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void TeamBase_GetValue_Success()
        {
            //given
            Team team = new Team("Mercedes-AMG Petronas F1 Team", "Brackley, United Kingdom", "Toto Wolff");
            string actualValue = "";
            string expectedValue = "Brackley, United Kingdom";

            //when
            actualValue = team.TeamBase;

            //then
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void TeamChief_GetValue_Success()
        {
            //given
            Team team = new Team("Mercedes-AMG Petronas F1 Team", "Brackley, United Kingdom", "Toto Wolff");
            string actualValue = "";
            string expectedValue = "Toto Wolff";

            //when
            actualValue = team.TeamChief;

            //then
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void WorldChampionship_SetGetValue_Success()
        {
            //given
            Team team = new Team("Mercedes-AMG Petronas F1 Team", "Brackley, United Kingdom", "Toto Wolff");
            int expectedValue = 6;

            //when
            team.WorldChampionship = expectedValue;

            //then
            Assert.AreEqual(expectedValue, team.WorldChampionship);
        }

        [TestMethod]
        public void Drivers_SetGetValueOneDriver_Success()
        {
            //given
            Team team = new Team("Mercedes-AMG Petronas F1 Team", "Brackley, United Kingdom", "Toto Wolff");
            Driver driver = new Driver("Hamilton", "Lewis", 44);
            List<Driver> drivers = new List<Driver>();
            drivers.Add(driver);
            int expectedValue = 1;

            //when
            team.Drivers = drivers;

            //then
            Assert.AreEqual(expectedValue, team.Drivers.Count);
        }

        [TestMethod]
        public void Cars_GetSetValueOneCar_Success()
        {
            //given
            Team team = new Team("Mercedes-AMG Petronas F1 Team", "Brackley, United Kingdom", "Toto Wolff");
            Car car = new Car("Mercedes-Benz", "Mercedes-Benz");
            List<Car> cars = new List<Car>();
            cars.Add(car);
            int expectedValue = 1;

            //when
            team.Cars = cars;

            //then
            Assert.AreEqual(expectedValue, team.Cars.Count);
        }
    }
}
