﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FormulaOneBusiness;

namespace TestFormulaOneBusiness
{
    [TestClass]
    public class TestCar
    {
        [TestMethod]
        public void Brand_GetValue_Success()
        {
            //given
            Car car = new Car("Mercedes-Benz", "Mercedes-Benz");
            string actualValue = "";
            string expectedValue = "Mercedes-Benz";

            //when
            actualValue = car.Brand;

            //then
            Assert.AreEqual(expectedValue, actualValue);

        }

        [TestMethod]
        public void PowerUnit_GetValue_Success()
        {
            //given
            Car car = new Car("Mercedes-Benz", "Mercedes-Benz");
            string actualValue = "";
            string expectedValue = "Mercedes-Benz";

            //when
            actualValue = car.PowerUnit;

            //then
            Assert.AreEqual(expectedValue, actualValue);

        }
    }
}
