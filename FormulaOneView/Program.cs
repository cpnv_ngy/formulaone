﻿using FormulaOneBusiness;
using System;
using System.Collections.Generic;

namespace FormulaOne
{
    class Program
    {
        private static List<Team> teams = new List<Team>();

        private static void Main(string[] args)
        {
            Init();
            DisplayTeams();
        }

        private static void Init()
        {
            //Create Mercedes Team
            Team mercedesTeam = new Team("Mercedes-AMG Petronas F1 Team", "Brackley, United Kingdom", "Toto Wolff");
            Driver mercredesDriverLeader = new Driver("HAMILTON", "Lewis", 44);
            Driver mercredesDriverTrainee = new Driver("BOTTAS", "Vallteri", 77);
            List<Driver> mercedesDrivers = new List<Driver>();
            mercedesDrivers.Add(mercredesDriverLeader);
            mercedesDrivers.Add(mercredesDriverTrainee);
            Car mercedesCarLeader = new Car("Mercedes", "Mercedes");
            Car mercedesCarTrainee = new Car("Mercedes", "Mercedes");
            List<Car> mercedesCars = new List<Car>()
            {
                mercedesCarLeader,
                mercedesCarTrainee
            };

            mercedesTeam.Cars = mercedesCars;
            mercedesTeam.Drivers = mercedesDrivers;

            //Create Red Bull Team
            Team redbullTeam = new Team("Aston Martin Red Bull Racing", "Milton Keynes, United Kingdom", "Christian Horner");
            Driver redbullDriverLeader = new Driver("VERSTAPPEN", "Max", 33);
            Driver redbullDriverTrainee = new Driver("ALBON", "Alexander", 23);
            List<Driver> redbullDrivers = new List<Driver>();
            redbullDrivers.Add(redbullDriverLeader);
            redbullDrivers.Add(redbullDriverTrainee);
            Car redbullCarLeader = new Car("Red Bull", "Honda");
            Car redbullCarTrainee = new Car("Red Bull", "Honda");
            List<Car> redbullCars = new List<Car>()
            {
                redbullCarLeader,
                redbullCarTrainee
            };

            redbullTeam.Cars = redbullCars;
            redbullTeam.Drivers = redbullDrivers;

            //Add team to teams
            teams.Add(mercedesTeam);
            teams.Add(redbullTeam);
        }

        private static void DisplayTeams()
        {
            foreach (Team teamToDisplay in teams)
            {
                Console.WriteLine("*Team****************");
                
                Console.WriteLine(teamToDisplay.FullTeamName);
                Console.WriteLine("-----Drivers");
                foreach (Driver driverToDisplay in teamToDisplay.Drivers)
                {
                    Console.WriteLine(driverToDisplay.Lastname + " " + driverToDisplay.Name);
                }
                Console.WriteLine("-----Cars");
                foreach (Car carToDisplay in teamToDisplay.Cars)
                {
                    Console.WriteLine(carToDisplay.Brand + " - " + carToDisplay.PowerUnit);
                }
                Console.WriteLine("*****************");
            }
        }
    }
}
